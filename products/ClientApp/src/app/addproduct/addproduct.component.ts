import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.scss']
})
/** addproduct component*/
export class AddproductComponent {
  employeeForm: FormGroup;
  title: string = "Create";
  productID: number;
  errorMessage: any;
  cityList: Array<any> = [];

  constructor(private _fb: FormBuilder, private _avRoute: ActivatedRoute,
    private _Productervice: ProductService, private _router: Router) {
    if (this._avRoute.snapshot.params["id"]) {
      this.productID = this._avRoute.snapshot.params["id"];
    }

    this.employeeForm = this._fb.group({
      productID: 0,
      name: ['', [Validators.required]],
      price: ['', [Validators.required]],
      description: ['', [Validators.required]],
    })
  }

  ngOnInit() {


    if (this.productID > 0) {
      this.title = "Edit";
      this._Productervice.getProductById(this.productID)
        .subscribe(resp => this.employeeForm.setValue(resp)
          , error => this.errorMessage = error);
    }

  }

  save() {

    if (!this.employeeForm.valid) {
      return;
    }

    if (this.title == "Create") {
      this._Productervice.createProduct(this.employeeForm.value)
        .subscribe((data) => {
          this._router.navigate(['/fetch-product']);
        }, error => this.errorMessage = error)
    }
    else if (this.title == "Edit") {
      this._Productervice.updateProduct(this.productID ,this.employeeForm.value)
        .subscribe((data) => {
          this._router.navigate(['/fetch-product']);
        }, error => this.errorMessage = error)
    }
  }

  cancel() {
    this._router.navigate(['/fetch-product']);
  }

  get name() { return this.employeeForm.get('name'); }
  get price() { return this.employeeForm.get('price'); }
  get description() { return this.employeeForm.get('description'); }
}
