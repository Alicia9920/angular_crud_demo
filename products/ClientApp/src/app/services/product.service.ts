import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class ProductService {
  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
  }
  getAllProduct(): Observable<Product[]> {
    return this.http.get<Product[]>(this.baseUrl + 'api/Products');
  }
  getProductById(productID: number): Observable<Product> {
    return this.http.get<Product>(this.baseUrl + 'api/Products/' + productID);
  }
  createProduct(Product: Product): Observable<Product> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.post<Product>(this.baseUrl + 'api/Products/',
      Product, httpOptions);
  }
  updateProduct(productID: number, Product: Product): Observable<Product> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.put<Product>(this.baseUrl + 'api/Products/' + productID,
      Product, httpOptions);
  }
  deleteProductById(productID: number): Observable<number> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.delete<number>(this.baseUrl + 'api/Products/' + productID,
      httpOptions);
  }
}
