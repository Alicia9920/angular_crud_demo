﻿using products.Data;
using products.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace products.Repositories
{
    public class ProductRepository : IRepository<Product, long>
    {

        public ApplicationContext context { get; set; }

        public IEnumerable<Product> Get()
        {
            return context.Products.ToList();
        }

        public Product Get(long id)
        {
            return context.Products.Find(id);
        }

        public void Add(Product entity)
        {
            context.Products.Add(entity);
            context.SaveChanges();
        }

        public void Remove(long id)
        {
            var obj = context.Products.Find(id);
            context.Products.Remove(obj);
            context.SaveChanges();
        }

        public void Update(Product entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChangesAsync();
        }
    }
}
