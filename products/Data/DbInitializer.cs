﻿using JsonNet.PrivateSettersContractResolvers;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using products.Data;
using products.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace products.Data
{
    public static class DbInitializer
    {
        public static void Initialize(ApplicationContext context)
        {
            context.Database.EnsureCreated();
        }
        public static void Seedit(string jsonData,
                                      IServiceProvider serviceProvider)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                ContractResolver = new PrivateSetterContractResolver()
            };
            var jsondate =
             JsonConvert.DeserializeObject<List<Product>>(
               jsonData, settings);
            using (
             var serviceScope = serviceProvider
               .GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope
                              .ServiceProvider.GetService<ApplicationContext>();
                if (!context.Products.Any())
                {
                    context.AddRange(jsondate);
                    context.SaveChanges();
                }
            }
        }
    }
}
