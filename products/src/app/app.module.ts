import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { FetchproductsComponent } from './fetchproducts/fetchproducts.component';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { ProductService } from './services/product.service';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    FetchproductsComponent,
    AddproductComponent,
    FooterComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    CommonModule,
    HttpModule,  
    ReactiveFormsModule,  
    RouterModule.forRoot([
      { path: '', redirectTo: 'fetch-product', pathMatch: 'full' },
      { path: 'fetch-product', component: FetchproductsComponent },
      { path: 'register-product', component: AddproductComponent },
      { path: 'product/edit/:id', component: AddproductComponent },
      { path: '**', redirectTo: 'fetch-product' }  
    ])
  ],
  providers: [ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
