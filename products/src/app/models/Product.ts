interface Product {
  productID: number;
  name: string;
  price: string;
  description: string;
}
