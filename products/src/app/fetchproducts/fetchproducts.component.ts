import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { ProductService } from '../services/product.service';

@Component({
    selector: 'app-fetchproducts',
    templateUrl: './fetchproducts.component.html',
    styleUrls: ['./fetchproducts.component.scss']
})
/** fetchproducts component*/
export class FetchproductsComponent {
    /** fetchproducts ctor */

    public empList: Product[];

  constructor(public http: Http, private _router: Router, private _ProductService: ProductService) {
    this.getProducts();
  }

  getProducts() {
    this._ProductService.getAllProduct().subscribe(
      data => this.empList = data
    )
  }

  delete (productID) {
    var ans = confirm("Do you want to delete product with Id: " + productID);
    if (ans) {
      this._ProductService.deleteProductById(productID).subscribe((data) => {
        this.getProducts();
      }, error => console.error(error))
    }
  }
}


