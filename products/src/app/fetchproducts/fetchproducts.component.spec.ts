import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { FetchproductsComponent } from './fetchproducts.component';

let component: FetchproductsComponent;
let fixture: ComponentFixture<FetchproductsComponent>;

describe('fetchproducts component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ FetchproductsComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(FetchproductsComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});
