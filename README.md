### Getting Started

This project contains is a demo app that shows CRUD with REST API build in .Net Core. It is a simple single page app that shows a list of products with the option to add and edit product details

#### The project has 3 parts

Angular CLI which is on top of node js
ASP.NET Core for APIS
SQL server for database parts

#### About Platform Used
Angular Version Used 8.0.0 and CLI version 8.0.2

Microsoft Visual Studio Community 2019
Link to download Microsoft Visual Studio Community 2017: - https://visualstudio.microsoft.com/vs/

Microsoft SQL Server 2016
Link to download SQL Server Express: - https://www.microsoft.com/en-in/download/details.aspx?id=29062

Visual Studio Code
Link to download Visual studio code: - https://code.visualstudio.com/download

TypeScript Versions
tsc --version Version 3.4.5


#### How to run the Project
1. Clone repository to your local machine
2. Open "products.sln" under "products" Directory and Run Web API Project which will run the angular side as well. 
